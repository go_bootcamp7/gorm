package main

import (
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Stores struct {
	gorm.Model
	Name     string
	Address  string
	Products []*Product `gorm:"many2many:stores_products"`
}

type Product struct {
	gorm.Model
	Name   string
	Models string
	Price  float32
}

type Student struct {
	Id        int64 `gorm:"primaryKey"`
	Name      string
	Age       int64
	Email     string
	Addresses []*Address `gorm:"foreignKey:StudentId;references:Id"`
}

type Address struct {
	Id        int64 `gorm:"primaryKey"`
	StudentId int64
	Country   string
	Street    string
}

const (
	Host     = "localhost"
	Port     = 5432
	User     = "developer"
	Password = "2002"
	DbName   = "gormdb"
)

func main() {
	conndb := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		Host, Port, User, Password, DbName,
	)

	db, err := gorm.Open(postgres.Open(conndb), &gorm.Config{})
	if err != nil {
		log.Println("Error connDb: ", err)
	}

	// InserInfo(db)
	SelectInfo(db)

}

// func InserInfo(db *gorm.DB) {
// 	data := &Student{
// 		Id:    1,
// 		Name:  "Davlat",
// 		Age:   24,
// 		Email: "jamila@gmail.com",
// 		Addresses: []*Address{
// 			{
// 				Id:        1,
// 				StudentId: 1,
// 				Country:   "Uzbekistan",
// 				Street:    "street",
// 			},
// 			{
// 				Id:        1,
// 				StudentId: 1,
// 				Country:   "Uzbekistan",
// 				Street:    "street amir temur",
// 			},
// 		},
// 	}
// 	db.AutoMigrate(&Student{}, &Address{})
// 	db.Create(&data)
// 	fmt.Println("successfully insert student info: ", data, &data.Addresses)
// }

func SelectInfo(db *gorm.DB) {
	result := Student{}
	err := db.Raw(`SELECT id, name, age, email FROM students WHERE id=$1`, 1).Scan(&result)
	if err != nil {
		fmt.Println("Error while select info", err)
	}
	fmt.Println("Student info: ", result)
	res := Address{}
	err = db.Raw(`SELECT id, student_id, country, street FROM addresses WHERE student_id=$1`, 1).Scan(&res)
	if err != nil {
		fmt.Println("Error select address info: ", err)
	}
	fmt.Println(res)
}
